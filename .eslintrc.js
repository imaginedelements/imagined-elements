const path = require("path");

module.exports = {
  extends: path.resolve(
    __dirname,
    "./packages/configs/eslint-config-imagined/node"
  )
};
