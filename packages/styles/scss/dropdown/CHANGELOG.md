# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.2.0 (2019-05-24)


### Features

* **PRI-9482:** Minor visual updates to existing components ([1098ac8](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/1098ac8))






## [1.1.1](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-dropdown@1.1.0...@cadent/sass-dropdown@1.1.1) (2019-05-24)

**Note:** Version bump only for package @cadent/sass-dropdown





# 1.1.0 (2019-05-24)


### Features

* **PRI-9482:** Minor visual updates to existing components ([1098ac8](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/1098ac8))
