# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.4.0](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-inputs@1.3.1...@cadent/sass-inputs@1.4.0) (2019-05-28)


### Features

* **PRI-9516:** Create "Select" universal component ([57584c6](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/57584c6))






## [1.3.1](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-inputs@1.3.0...@cadent/sass-inputs@1.3.1) (2019-05-28)


### Bug Fixes

* **PRI-9324:** Fix a minor bug that InputCollection has been multiple selected ([bef5de3](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/bef5de3))






# [1.3.0](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-inputs@1.2.0...@cadent/sass-inputs@1.3.0) (2019-05-24)


### Features

* **PRI-9324:** Create checkbox universal component ([5a53f31](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/5a53f31))






# 1.2.0 (2019-05-24)


### Features

* **PRI-9482:** Minor visual updates to existing components ([1098ac8](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/1098ac8))






## [1.1.5](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-inputs@1.1.4...@cadent/sass-inputs@1.1.5) (2019-05-24)

**Note:** Version bump only for package @cadent/sass-inputs
