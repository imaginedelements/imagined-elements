# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.2.0](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-forms@1.1.0...@cadent/sass-forms@1.2.0) (2019-05-28)


### Features

* **PRI-9516:** Create "Select" universal component ([57584c6](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/57584c6))






# 1.1.0 (2019-05-24)


### Features

* **PRI-9482:** Minor visual updates to existing components ([1098ac8](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/1098ac8))






## [1.0.7](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-forms@1.0.6...@cadent/sass-forms@1.0.7) (2019-05-24)

**Note:** Version bump only for package @cadent/sass-forms
