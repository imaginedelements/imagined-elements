# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.7](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-modal@1.0.6...@cadent/sass-modal@1.0.7) (2019-05-24)


### Bug Fixes

* **PRI-9586:** Remove top padding for modal body ([118d868](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/commits/118d868))






## 1.0.6 (2019-05-24)

**Note:** Version bump only for package @cadent/sass-modal






## [1.0.5](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-modal@1.0.4...@cadent/sass-modal@1.0.5) (2019-05-24)

**Note:** Version bump only for package @cadent/sass-modal
