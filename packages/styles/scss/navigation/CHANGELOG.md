# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.7 (2019-05-24)

**Note:** Version bump only for package @cadent/sass-navigation






## [1.0.6](http://devvm01tfs03.telamericamedia.com:8080/tfs/CMWPA/Cadent%20Shared%20Resources/_git/cadent-components/compare/@cadent/sass-navigation@1.0.5...@cadent/sass-navigation@1.0.6) (2019-05-24)

**Note:** Version bump only for package @cadent/sass-navigation
