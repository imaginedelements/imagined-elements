const path = require("path");

module.exports = {
  extends: [path.resolve(__dirname, "./_typescript.js")],
  parserOptions: {
    project: "tsconfig.node.json"
  }
};
