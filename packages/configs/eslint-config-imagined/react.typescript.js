const path = require("path");

module.exports = {
  extends: [path.resolve(__dirname, "./_typescript.js")],
  ecmaFeatures: { jsx: true },
  parserOptions: {
    project: "tsconfig.react.json"
  }
};
