const path = require("path");

module.exports = {
  parser: "@typescript-eslint/parser",
  extends: [
    "plugin:@typescript-eslint/recommended",
    "prettier/@typescript-eslint",
    path.resolve(__dirname, "./_base.js")
  ],
  parserOptions: {
    tsconfigRootDir: path.resolve(
      __dirname,
      "./node_modules/@imagined/config-typescript"
    )
  },
  settings: {
    "import/resolver": {
      node: {
        extensions: [".js", ".ts", ".tsx", ".svg", ".css"]
      }
    }
  }
};
