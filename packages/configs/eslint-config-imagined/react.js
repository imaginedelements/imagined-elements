const path = require("path");

module.exports = {
  parser: "babel-eslint",
  extends: ["airbnb", path.resolve(__dirname, "./_base.js")],
  plugins: ["react"],
  settings: {
    react: {
      version: "latest"
    }
  }
};
