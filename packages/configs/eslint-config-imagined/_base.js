module.exports = {
  extends: [
    "airbnb-base",
    "prettier",
    "plugin:prettier/recommended",
    "plugin:jest/recommended"
  ],
  plugins: ["prettier"],
  parserOptions: {
    sourceType: "module"
  },
  env: { browser: true, es6: true, node: true, jest: true },
  rules: {
    "prettier/prettier": "error",
    "no-debugger": 1
  }
};
