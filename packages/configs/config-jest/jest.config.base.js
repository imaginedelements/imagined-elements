module.exports = {
  roots: ["<rootDir>/src"],
  preset: "ts-jest",
  transform: {
    "^.+\\.(js|jsx|ts|tsx)?$": "ts-jest"
  },
  testPathIgnorePatterns: ["<rootDir>/node_modules/"],
  testRegex: "(/src/.*.(test|spec)).(ts|tsx|js|jsx?)$",
  moduleFileExtensions: ["js", "jsx", "json", "ts", "tsx"],
  verbose: true,
  coverageThreshold: {
    global: {
      branches: 50,
      functions: 50,
      lines: 50,
      statements: 50
    }
  }
};
