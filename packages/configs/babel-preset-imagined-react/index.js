module.exports = api => {
  api.cache(true);

  const presets = [
    "@babel/preset-react",
    [
      "@babel/preset-env",
      {
        modules: false,
        targets: {
          browsers: "last 2 versions, > 5%, safari tp"
        }
      }
    ],
    "@imagined/babel-preset-cadent-base"
  ];

  return {
    presets
  };
};
