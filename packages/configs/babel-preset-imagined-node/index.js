module.exports = api => {
  api.cache(true);

  const presets = [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current"
        }
      }
    ],
    "@imagined/babel-preset-cadent-base"
  ];

  return {
    presets
  };
};
