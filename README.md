## CLI Features

### Opinionated

- Create new vue template
- Create new react + redux template
- Create new Gatsby template
- Create new Vue (static site template)
- Create a new style system using the `@imagined/styles/scss` directory
- Create static CSS files from the `@imagined/styles/scss` directory

## Build Features

- Build CSS files
-
